# Messaging Queues

## Table of contents

- What are Messaging Queues?
- Why they are used?
- What are the popular tools?
- What is Enterprise Message Bus?
- Resources

## What are Messaging Queues?

**Message queuing** allows applications to communicate by sending messages to each other. The message queue provides temporary message storage when the destination program is busy or not connected.

A message queue is a form of **asynchronous** **service-to-service communication**. It is a named destination to which messages can be sent. Messages accumulate on queues until they are retrieved by programs that service those queues. Messages are stored on the queue until they are processed and deleted. Each message is processed only once, by a single consumer.

Now, let us understand what does it mean for a process to be **Asynchronous**.
In an Asynchronous process, The producer sends the requests for a process to be done, If the consumer process is busy with some other task. The producer request is sent to the message queue. The processes are in the message queue until the process is requested by the receiver and is completed.

![async](https://d1.awsstatic.com/product-marketing/Messaging/sqs_seo_queue.1dc710b63346bef869ee34b8a9a76abc014fbfc9.png)

> Many producers and consumers can use the queue, but each message is processed only once, by a single consumer. For this reason, this messaging pattern is often called one-to-one, or point-to-point, communications.

**Synchronous processes** are those in which the producer sends the requests for a process to be done, and waits until it receives back the process. This happens even if the consumer process is busy with some other task.

<p align="center">
  <img width="460" src="https://miro.medium.com/max/587/1*Y41dOkntUbR3I4UCJBx9Xg.png">
</p>

## Why are Message Queues used?

Imagine that you have a web service that receives many requests every second, where no request can get lost, and all requests need to be processed by a function that has a high throughput. In other words, the web service always has to be highly available and ready to receive a new request instead of being locked by the processing of previously received requests.

In this case, Asynchronous messaging queues help us to do the tasks requested and also do the other tasks meanwhile. When the producer sends the process, It checks whether the consumer process is under any task. If not under a task, It will schedule the task to the consumer process. If the consumer process is undergoing a task, The process gets scheduled to the message queue and waits for the process to complete.

Then imagine that the business and workload are growing and the system needs to be scaled up. All that needs to be done is to add more consumers to work off the queues faster.

<p align="center">
  <img width="460" src="https://www.cloudamqp.com/img/blog/thumb-mq.jpg">
</p>

## What are the popular tools?

Kafka, RabbitMQ, Amazon SQS, Celery, and ActiveMQ are the most popular tools in the category **Message Queue**.

1. **Apache Kafka** - It is a message queuing system with a couple of twists. It offers low-latency message processing just like a great message queue, along with high availability and fault tolerance.
2. **RabbitMQ** - It is the most widely deployed open-source message queue, lightweight, and easy to deploy on-premises and in the cloud. RabbitMQ supports multiple messaging protocols. RabbitMQ can be deployed in distributed systems to meet high-scale, high-availability requirements.
3. **Amazon Simple Queue Service (SQS)** - It is a fully managed message queuing service that enables you to scale microservices, distributed systems, and serverless applications.
4. **Celery** - It is a simple, flexible, and reliable distributed system to process vast amounts of messages, while providing operations with the tools required to maintain such a system. It is a task queue with a focus on **real-time processing**, while also supporting **task scheduling**.
5. **ActiveMQ** - It is the most popular open-source, multi-protocol, Java-based message broker. It supports industry-standard protocols so users get the benefits of client choices across a broad range of languages and platforms like in JavaScript, C, C++, Python, .Net, and more.

## What is Enterprise Message Bus?

An enterprise service bus (ESB) is a software platform used to distribute work among connected components of an application. It is designed to provide a uniform means of moving work, offering applications the ability to connect to the ESB and subscribe to messages based on simple structural and business policy rules.

<p align="center">
  <img width="460" src="https://cdn.ttgtmedia.com/rms/onlineImages/microservices-esb_diagram.jpg">
</p>

## Why do we use an ESB?

- Makes it easy to change or add components.
- Provide a convenient place to enforce security and compliance, logging, and monitoring.
- Can provide load balancing to improve performance. This mainly helps because the resources are intelligently allocated by removing duplicates.
- Can provide failover support in case of a component or resource failure.

However, A common challenge associated with the ESB concept is the lack of a single accepted standard for features or behavior.

## References

- [Asynchroonous vs Synchronous Processes](https://stackoverflow.com/questions/748175/asynchronous-vs-synchronous-execution-what-is-the-main-difference)
- [Introduction to Message Queue](https://www.ibm.com/docs/en/ibm-mq/9.0?topic=overview-introduction-message-queuingx)
- [Amazon SQS](https://aws.amazon.com/sqs/)
- [Celery Message Queue](https://docs.celeryproject.org/en/stable/)
- [Apache ActiveMQ](https://activemq.apache.org/)
- [Enterprise Message Bus](https://searchapparchitecture.techtarget.com/definition/Enterprise-Service-Bus-ESB)
- [Markdown](https://guides.github.com/features/mastering-markdown/)
